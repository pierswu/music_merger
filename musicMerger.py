import simpleProgressbar
import time
import mergeSong
import shutil
import os
import os.path as path

from tkinter.filedialog import askdirectory
import tkinter as tk

top = tk.Tk() 
top.withdraw()
file_path = askdirectory()

sourceDir = file_path
targetDir = path.join(sourceDir,"target")


if not path.exists(sourceDir) or not path.isdir(sourceDir):
    raise Exception("源目录不存在")

songfiles = [path.join(sourceDir, fileName).replace("\\","/") for fileName in os.listdir(
    sourceDir) if fileName.endswith('.mp3')]

if path.exists(targetDir):
    shutil.rmtree(targetDir)
os.mkdir(targetDir)

print("歌曲列表")
i = 1
for songFile in songfiles:
    fileName = songFile[songFile.rfind('/')+1:]
    print("%d) %s" % (i, fileName))
    i = i+1

print("\n开始合并")
for songFile in songfiles:
    fileName = songFile[songFile.rfind('/')+1:]
    sp = simpleProgressbar.SimpleProgress(
        text="%s : " % (fileName))
    mergeSong.merge(songFile, targetDir, sp)

os.system("pause")
