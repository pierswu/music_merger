import progressbar
import time
import threading


class SimpleProgress(object):
    def __init__(self, *args, **kwargs):
        super().__init__()

        self._text = kwargs.get('text', 'Progress :')
        widgets = [
            self._text,
            progressbar.Percentage(),
            ' ', progressbar.Bar(marker='#', left='[', right=']'),
            ' ', progressbar.Timer()
        ]
        self._total = kwargs.get('total', 100)
        self._progressbar = progressbar.ProgressBar(
            widgets=widgets, maxval=self._total).start()
        self._work = 0
        self._lock = threading.Lock()

    def worked(self, work=1):
        self._work += work
        if self._work>self._total:
            return 0
        if self._work >= self._total:
            self.finish()
        else:
            self._progressbar.update(self._work)

    def finish(self):
        self._work=self._total
        self._progressbar.update(self._total)
        self._progressbar.finish()

    def isFinished(self):
        return self._work==self._total

    def threadWorked(self, sec):
        _this = self
        sleepTime = sec / 100
        def run():
            for i in range(1,100):
                if _this.isFinished():
                    return 0
                _this.worked()
                time.sleep(sleepTime)
        
        t = threading.Thread(target=run)
        t.start()