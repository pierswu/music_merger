import os
import os.path as path
import shutil
import splitSilence
from subprocess import run


def merge(sourceFile, targetDir, pbar=None):
    targetDir = targetDir if targetDir.endswith("/") else targetDir+"/"

    if not path.exists(sourceFile):
        raise Exception("源文件不存在不存在:%s" % (sourceFile))

    fileName = sourceFile[sourceFile.rfind('/')+1:]
    name = fileName[:fileName.index(".")]

    targetFilesDir = "%s%s加长版" % (targetDir, name)

    os.mkdir(targetFilesDir)

    targetFile = path.join(targetFilesDir, fileName)
    targetSound = splitSilence.split(sourceFile, targetFile, pbar=pbar)

    overTime = int(30*60*1000/len(targetSound))

    for i in range(1, overTime):
        shutil.copy(targetFile, "%s/%s.mp3" % (targetFilesDir, i))
    run("7z -tZip a \"%s.mp3\" \"%s*\" -mx0" %
        (targetFilesDir, targetFilesDir), shell=True, capture_output=True)
    shutil.rmtree(targetFilesDir)
    pbar and pbar.finish()
