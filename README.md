# music_merger

#### 项目介绍
将一段MP3加长成大概三十分钟的脚本，并且去掉首尾空白的部分。

#### 依赖

- 安装并配置ffmpeg：http://ffmpeg.org/
- 安装pydub

```shell
pip install pydub
```