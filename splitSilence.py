from pydub import AudioSegment
from pydub.silence import detect_silence


def split(sourceFile, targetFile, format="mp3", pbar=None):
    sourceSound = AudioSegment.from_file(sourceFile, format=format)
    pbar.threadWorked(len(sourceSound)/12000)
    silences = detect_silence(sourceSound, 300, -35, 5)

    start = 0
    end = len(sourceSound)
    if len(silences) >= 2:
        start = silences[0][1]
        end = silences[-1][0]
    elif len(silences) == 1:
        start = silences[0][1]

    targetSound = sourceSound[start:end]
    targetSound.export(targetFile, format=format)
    return targetSound
